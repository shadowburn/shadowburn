defmodule CharactersTest do
  use Common.DataCase
  
  alias Common.Character
  alias Common.Characters
  alias Common.{Realms, Realm}

  alias Common.ServerRepo

  def create_character(character_name, account_id \\ 1), do: Characters.create_character(account_id, character_name, 1, 1, 0, 1, 1, 1, 1, 1)

  describe "when a character is deleted" do
    test "is_deleted should be true" do
      assert {:ok, %Character{id: id1}} = create_character("Annie")
      assert %Character{id: ^id1, is_deleted: :false} = ServerRepo.get(Character, id1)
      assert :ok = Characters.delete_character(id1, 1)
      assert %Character{id: ^id1, is_deleted: :true} = ServerRepo.get(Character, id1)
    end

    test "character should not be in character count" do
      assert {:ok, %Character{id: id}} = create_character("Annie")
      assert {:ok, %Character{id: _}} = create_character("Bernice")
      assert [{%Realm{population: 1}, 2}] = Realms.get_realms_with_character_count(1)
      assert :ok = Characters.delete_character(id, 1)
      assert [{%Realm{population: 1}, 1}] = Realms.get_realms_with_character_count(1)
    end

    test "character should not be in character list" do
      assert {:ok, %Character{id: id1}} = create_character("Annie")
      assert {:ok, %Character{id: id2}} = create_character("Bernice")
      assert [%Character{id: ^id1}, %Character{id: ^id2}] = Characters.get_characters_for_account(1)
      assert :ok = Characters.delete_character(id1, 1)
      assert [%Character{id: ^id2}] = Characters.get_characters_for_account(1)
    end

    test "character should not be counted for population" do
      assert {:ok, %Character{id: id}} = create_character("Annie")
      assert {:ok, %Character{id: _}} = create_character("Bernice", 2)
      assert [{%Realm{population: 2}, _}] = Realms.get_realms_with_character_count(1)
      assert :ok = Characters.delete_character(id, 1)
      assert [{%Realm{population: 1}, _}] = Realms.get_realms_with_character_count(1)
    end

    test "a new character can be created with the same name" do
      assert {:ok, %Character{id: id}} = create_character("Annie")
      assert {:error, :name_taken} = create_character("Annie")
      assert :ok = Characters.delete_character(id, 1)
      assert {:ok, %Character{id: _}} = create_character("Annie")    
    end
  end

  describe "when a new character is created" do
    test "should error if account_id is missing" do
      assert {:error, changeset} = Characters.new_character_changeset(nil, "Annie", 1, 1, 0, 1, 1, 1, 1, 1) |> ServerRepo.insert
      assert "can't be blank" in errors_on(changeset).account_id    
    end

    test "should error if race can't be found" do
      assert {:error, changeset} = Characters.new_character_changeset(1, "Annie", 99, 1, 0, 1, 1, 1, 1, 1) |> ServerRepo.insert
      assert "Race 99 not found" in errors_on(changeset).race
    end

    test "should work with valid information" do
      assert {:ok, char} = Characters.new_character_changeset(1, "Annie", 1, 1, 0, 1, 1, 1, 1, 1) |> ServerRepo.insert
      character_name = ServerRepo.get(Character, char.id).name
      assert character_name == char.name
    end

    test "should increment realm population" do
      assert [{%Realm{population: 0}, _}] = Realms.get_realms_with_character_count(1)
      
      assert {:ok, %Character{id: id1}} = create_character("Annie")
      assert [{%Realm{population: 1}, _}] = Realms.get_realms_with_character_count(1)

      assert {:ok, %Character{id: id2}} = create_character("Bernice")
      assert [{%Realm{population: 1}, _}] = Realms.get_realms_with_character_count(1)
      
      assert {:ok, %Character{id: id3}} = create_character("Claire", 2)
      assert [{%Realm{population: 2}, _}] = Realms.get_realms_with_character_count(1)
      
      assert :ok = Characters.delete_character(id2, 1)
      assert [{%Realm{population: 2}, _}] = Realms.get_realms_with_character_count(1)
      
      assert :ok = Characters.delete_character(id1, 1)
      assert [{%Realm{population: 1}, _}] = Realms.get_realms_with_character_count(1)
      
      assert :ok = Characters.delete_character(id3, 2)
      assert [{%Realm{population: 0}, _}] = Realms.get_realms_with_character_count(1)      
    end

    test "should have proper character count" do
      assert [{%Realm{}, nil}] = Realms.get_realms_with_character_count(1)
      
      assert {:ok, %Character{id: id1}} = create_character("Annie")
      assert [{%Realm{}, 1}] = Realms.get_realms_with_character_count(1)

      assert {:ok, %Character{id: id2}} = create_character("Bernice")
      assert [{%Realm{}, 2}] = Realms.get_realms_with_character_count(1)

      assert {:ok, %Character{id: id3}} = create_character("Claire")
      assert [{%Realm{}, 3}] = Realms.get_realms_with_character_count(1)

      assert :ok = Characters.delete_character(id1, 1)
      assert [{%Realm{}, 2}] = Realms.get_realms_with_character_count(1)
      
      assert :ok = Characters.delete_character(id2, 1)
      assert [{%Realm{}, 1}] = Realms.get_realms_with_character_count(1)
      
      assert :ok = Characters.delete_character(id3, 1)
      assert [{%Realm{}, 0}] = Realms.get_realms_with_character_count(1)      
    end
  end
end