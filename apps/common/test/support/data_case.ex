defmodule Common.DataCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias Common.LoginRepo
      alias Common.ServerRepo

      import Ecto
      import Ecto.Changeset
      import Ecto.Query
      import Common.DataCase
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Common.LoginRepo)
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Common.ServerRepo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Common.LoginRepo, {:shared, self()})
      Ecto.Adapters.SQL.Sandbox.mode(Common.ServerRepo, {:shared, self()})
    end

    :ok
  end

  def errors_on(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Enum.reduce(opts, message, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end
end