defmodule Common.ServerRepo.Migrations.CreateCharacterTable do
  use Ecto.Migration

  def change do
    create table(:character) do
      add :name, :string, size: 12
      add :level, :integer, default: 1

      add :account_id, :integer

      add :race, :integer
      add :class, :integer

      add :gender, :integer
      add :skin, :integer
      add :face, :integer
      add :hair_style, :integer
      add :hair_color, :integer
      add :facial_hair, :integer
      
      add :zone, :integer
      add :map, :integer
      add :position_x, :real
      add :position_y, :real
      add :position_z, :real
      add :orientation, :real

      add :hide_helm, :boolean, default: :false
      add :hide_cloak, :boolean, default: :false
      add :ghost, :boolean, default: :false
      add :name_change, :boolean, default: :false
      add :reset_spells, :boolean, default: :false
      add :reset_talents, :boolean, default: :false

      add :total_time, :integer, default: 0

      add :is_deleted, :boolean, default: :false

      timestamps()
    end
    
    create index :character, [:account_id]
    create unique_index :character, [:name], where: "NOT name_change and NOT is_deleted", name: :unique_character_name

    create table(:character_race_template, primary_key: :race) do
      add :race, :integer

      add :zone, :integer
      add :map, :integer

      add :position_x, :real
      add :position_y, :real
      add :position_z, :real
      add :orientation, :real
    end
  end
end
