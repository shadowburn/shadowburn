# Script for populating the database. You can run it as:
#
#   mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#   Common.ServerRepo.insert!(%Common.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Common.ServerRepo
alias Common.Character.RaceTemplate

ServerRepo.insert!(%RaceTemplate{race: 1, map: 0, zone: 12, position_x: -8949.95, position_y: -132.193, position_z: 83.5312, orientation: 0.0})
ServerRepo.insert!(%RaceTemplate{race: 2, map: 1, zone: 14, position_x: -618.518, position_y: -4251.67, position_z: 38.718, orientation: 0.0})
ServerRepo.insert!(%RaceTemplate{race: 3, map: 0, zone: 1, position_x: -6240.32, position_y: 331.033, position_z: 382.758, orientation: 6.17716})
ServerRepo.insert!(%RaceTemplate{race: 4, map: 1, zone: 141, position_x: 10311.3, position_y: 832.463, position_z: 1326.41, orientation: 5.69632})
ServerRepo.insert!(%RaceTemplate{race: 5, map: 0, zone: 85, position_x: 1676.71, position_y: 1678.31, position_z: 121.67, orientation: 2.70526})
ServerRepo.insert!(%RaceTemplate{race: 6, map: 1, zone: 215, position_x: -2917.58, position_y: -257.98, position_z: 52.9968, orientation: 0.0})
ServerRepo.insert!(%RaceTemplate{race: 7, map: 0, zone: 1, position_x: -6240.32, position_y: 331.033, position_z: 382.758, orientation: 0.0})
ServerRepo.insert!(%RaceTemplate{race: 8, map: 1, zone: 14, position_x: -618.518, position_y: -4251.67, position_z: 38.718, orientation: 0.0})
