# Script for populating the database. You can run it as:
#
#   mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#   Common.LoginRepo.insert!(%Common.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Common.Realm
alias Common.Accounts

# Common.LoginRepo.insert!(%Realm{name: "Normal Realm 1"})
Common.LoginRepo.insert!(%Realm{name: "PVP Realm 1", realm_type: 1})

Accounts.create_account("TEST", "password")
Accounts.create_account("ADMIN", "password")