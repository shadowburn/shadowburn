defmodule Common.LoginRepo.Migrations.CreateAccountTable do
  use Ecto.Migration

  def change do
    create table(:account) do
      add :username, :string, size: 32
      add :password_hash, :string, size: 60

      add :salt, :string
      add :verifier, :string

      add :session, :string

      add :os, :string, size: 5
      add :last_ip, :string, size: 15
      add :locale, :string, size: 4
      add :last_login, :utc_datetime_usec

      add :failed_logins, :integer, default: 0

      add :ip_locked, :boolean, default: :false

      timestamps()
    end

    create unique_index :account, [:username]
  end
end
