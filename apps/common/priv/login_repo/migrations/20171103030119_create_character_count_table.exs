defmodule Common.LoginRepo.Migrations.CreateCharacterCountTable do
  use Ecto.Migration

  def change do
    create table(:realm_character_count, primary_key: :false) do
      add :account_id, references(:account), primary_key: :true
      add :realm_id, references(:realm), primary_key: :true

      add :character_count, :integer, default: 0
    end
  end
end
