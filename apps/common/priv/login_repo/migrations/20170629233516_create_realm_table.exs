defmodule Common.LoginRepo.Migrations.CreateRealmTable do
  use Ecto.Migration

  def change do
    create table(:realm) do
      add :name, :string, size: 32
      add :address, :string, size: 32, default: "127.0.0.1:8085"
      add :realm_type, :integer, default: 0
      add :flags, :integer, default: 0
      add :timezone, :integer, default: 0
      add :allowed_security_level, :integer, default: 0

      add :population, :integer, default: 0
      add :max_population, :integer, default: 1_000
      
      timestamps()
    end

    create unique_index :realm, [:name]
  end
end
