defmodule Common.LoginRepo.Migrations.CreateBanTables do
  use Ecto.Migration

  def change do
    create table(:ip_ban) do
      add :ip, :string, size: 15

      add :ban_date, :utc_datetime_usec
      add :expiration_date, :utc_datetime_usec

      add :reason, :string, size: 255

      add :is_active, :boolean, default: :true

      timestamps()
    end

    create index :ip_ban, [:ip]

    create table(:account_ban) do
      add :account_id, references(:account)

      add :ban_date, :utc_datetime_usec
      add :expiration_date, :utc_datetime_usec

      add :reason, :string, size: 255

      add :is_active, :boolean, default: :true

      timestamps()
    end
  end
end
