# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :common,
  ecto_repos: [Common.LoginRepo, Common.ServerRepo]

config :common, Common.LoginRepo,
  migration_timestamps: [type: :utc_datetime_usec]

config :common, Common.ServerRepo,
  migration_timestamps: [type: :utc_datetime_usec]

  # Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.

import_config "#{Mix.env}.exs"
