use Mix.Config

# Configure your database
config :common, Common.LoginRepo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "login_test",  
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox

config :common, Common.ServerRepo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "server_test",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox

config :bcrypt_elixir, :log_rounds, 4  

config :common,
  auto_ban_enabled: :false,
  auto_ban_max_failed_logins: 5,
  auto_ban_type: :ip, # :account, :ip, :account_and_ip
  auto_ban_duration_in_seconds: 300 # 0 - permanent