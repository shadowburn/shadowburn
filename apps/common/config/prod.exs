use Mix.Config

# Do not print debug messages in production
config :logger, level: :info

config :common,
  auto_ban_enabled: :false,
  auto_ban_max_failed_logins: 5,
  auto_ban_type: :ip, # :account, :ip, :account_and_ip
  auto_ban_duration_in_seconds: 300 # 0 - permanent