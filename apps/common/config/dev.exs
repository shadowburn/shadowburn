use Mix.Config

config :common, Common.LoginRepo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "login_dev",
  hostname: "localhost",
  pool_size: 10

config :common, Common.ServerRepo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "server_dev",
  hostname: "localhost",
  pool_size: 10

config :common,
  auto_ban_enabled: :false,
  auto_ban_max_failed_logins: 5,
  auto_ban_type: :ip, # :account, :ip, :account_and_ip
  auto_ban_duration_in_seconds: 300 # 0 - permanent