defmodule Common.Account do
  use Ecto.Schema

  schema "account" do
    field :username, :string

    field :password, :string, virtual: true
    field :password_hash, :string

    field :salt, :string
    field :verifier, :string

    field :session, :string

    field :os, :string
    field :last_ip, :string
    field :locale, :string
    field :last_login, :utc_datetime_usec

    field :failed_logins, :integer

    field :ip_locked, :boolean

    timestamps()
  end
end
