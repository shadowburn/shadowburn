defmodule Common.Accounts do
  import Binary, only: [reverse: 1]

  import Ecto.{Changeset}

  require Logger

  alias Common.Account
  alias Common.Bans
  alias Common.LoginRepo

  @n << 137, 75, 100, 94, 137, 225, 83, 91, 189, 173, 91, 139, 41, 6, 80, 83, 8, 1, 177, 142, 191, 191, 94, 143, 171, 60, 130, 135, 42, 62, 155, 183 >>
  @g << 7 >>

  def create_account(username, password) do
    %Account{}
    |> new_account_changeset(%{username: username, password: password})
    |> LoginRepo.insert()
  end

  def successful_login(%Account{} = account, attrs \\ %{}) do
    account
    |> cast(attrs, [:session, :os, :last_ip, :locale, :last_login])
    |> put_change(:failed_logins, 0)
    |> put_change(:last_login, DateTime.utc_now())
    |> LoginRepo.update()
  end

  def failed_login(%Account{} = acc, current_ip) do
    {:ok, account} = LoginRepo.update(change(acc, failed_logins: acc.failed_logins + 1))
    Bans.check_auto_ban(account, current_ip)
  end

  defp new_account_changeset(%Account{} = account, attrs) do
    account
    |> cast(attrs, [:username, :password])
    |> validate_required([:username, :password])
    |> clean_params()
    |> generate_auth_values()
  end

  defp clean_params(changeset) do
    changeset
    |> update_change(:username, &(String.upcase(&1)))
    |> update_change(:password, &(String.upcase(&1)))
  end

  defp generate_auth_values(changeset) do
    username = get_field(changeset, :username)
    password = get_field(changeset, :password)

    state = generate_stored_values(username, password)

    changeset
    |> put_change(:password_hash, state.password_hash)
    |> put_change(:salt, Base.encode16(state.salt))
    |> put_change(:verifier, Base.encode16(state.verifier))
  end

  def generate_stored_values(username, password) do
    default_state()
    |> generate_salt()
    |> calculate_x(username, password)
    |> calculate_v()
    |> generate_password_hash(password)
  end

  def generate_password_hash(state, password) do
    password_hash = Bcrypt.hash_pwd_salt(password)
    Map.merge(state, %{password_hash: password_hash})
  end

  def default_state() do
    %{n: @n, g: @g}
  end

  def generate_salt(state) do
    salt = :crypto.strong_rand_bytes(32)
    Map.merge(state, %{salt: salt})
  end

  def calculate_x(state, username, password) do
    hash = :crypto.hash(:sha, String.upcase(username) <> ":" <> String.upcase(password))
    x = reverse(:crypto.hash(:sha, state.salt <> hash))
    Map.merge(state, %{x: x, username: username})
  end

  def calculate_v(state) do
    verifier = :crypto.mod_pow(state.g, state.x, state.n)
    Map.merge(state, %{verifier: verifier})
  end

  def account_ip_locked(%Account{} = account, current_ip) do
    case account do
      %Account{ip_locked: :true, last_ip: ^current_ip} -> :ok
      %Account{ip_locked: :true, last_ip: _ip}         -> :ip_locked
      _                                                -> :ok
    end
  end

  def get_account(username) do
    account = LoginRepo.get_by(Account, username: username)
    case account do
      %Account{} -> {:ok, account}
      _          -> {:error, :account_unknown}
    end
  end
end
