defmodule Common.Realms do
  import Ecto.Query
    
  alias Common.LoginRepo
  alias Common.Realm
  alias Common.RealmCharacterCount

  def get_realms_with_character_count(account_id) do
    LoginRepo.all from r in Realm,
                  left_join: c in RealmCharacterCount, on: r.id == c.realm_id and c.account_id == ^account_id,
                  select: {r, c.character_count}
  end
end