defmodule Common.Realm do
  use Ecto.Schema

  schema "realm" do
    field :name, :string
    field :address, :string
   
    field :realm_type, :integer
    # Normal = 0
    # PVP    = 1
    # RP     = 6
    # RPPVP  = 8
    
    field :flags, :integer
    field :timezone, :integer
    field :allowed_security_level, :integer

    field :population, :integer
    field :max_population, :integer

    timestamps()
  end 
end