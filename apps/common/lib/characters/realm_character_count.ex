defmodule Common.RealmCharacterCount do
  use Ecto.Schema

  alias Common.Realm
  alias Common.Account

  @primary_key false
  schema "realm_character_count" do
    belongs_to :realm, Realm, primary_key: :true
    belongs_to :account, Account, primary_key: :true

    field :character_count, :integer
  end
end