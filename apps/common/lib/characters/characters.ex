defmodule Common.Characters do
  import Ecto.{Changeset, Query}
  
  require Logger

  alias Common.ServerRepo
  alias Common.LoginRepo

  alias Common.Character
  alias Common.Character.RaceTemplate
  alias Common.Realm
  alias Common.RealmCharacterCount

  def get_character(character_id, account_id), do: ServerRepo.get_by(Character, id: character_id, account_id: account_id, is_deleted: :false)

  def get_characters_for_account(account_id) do
    characters_for_account_query(account_id)
    |> ServerRepo.all
  end

  defp characters_for_account_query(account_id) do
    Character
    |> where([c], c.account_id == ^account_id and c.is_deleted == :false)
  end

  def delete_character(character_id, account_id) do
    character = ServerRepo.get(Character, character_id)
    case character do
      nil -> {:error, :character_not_found}
      %Character{account_id: ^account_id} -> 
        character
        |> change(%{is_deleted: :true})
        |> ServerRepo.update
        update_character_count_and_realm_population(account_id, :delete)
        :ok
      %Character{} -> {:error, :invalid_account}
    end
  end

  def create_character(account_id, name, race, class, gender, skin, face, hair_style, hair_color, facial_hair) do
    result = new_character_changeset(account_id, name, race, class, gender, skin, face, hair_style, hair_color, facial_hair)
    |> ServerRepo.insert()

    case result do
      {:ok, character} ->
        update_character_count_and_realm_population(account_id, :create)
        {:ok, character}
      {:error, changeset} ->
        # cond do
        #   changeset.errors
        # end
        case changeset.errors do
          [name: {"has already been taken", _}] -> {:error, :name_taken}
          [name: {_, [_, validation: :length, min: _]}] -> {:error, :name_too_short}
          [name: {_, [_, validation: :length, max: _]}] -> {:error, :name_too_long}
          [name: {_, [validation: :required]}] -> {:error, :name_missing}
          [name: {_, [validation: :format]}] -> {:error, :name_invalid}
          _ ->  {:error, :failed}
        end
    end
  end

  def new_character_changeset(account_id, name, race, class, gender, skin, face, hair_style, hair_color, facial_hair) do
    attrs = %{account_id: account_id, name: name, 
              race: race, class: class, 
              gender: gender, skin: skin, face: face,
              hair_style: hair_style, hair_color: hair_color,
              facial_hair: facial_hair}
    %Character{}
    |> cast(attrs, [:account_id, :name, :race, :class, :gender, :skin, :face, :hair_style, :hair_color, :facial_hair])
    |> validate_required([:account_id, :name, :race, :class, :gender, :skin, :face, :hair_style, :hair_color, :facial_hair])
    |> validate_length(:name, min: 2, max: 12)
    |> validate_format(:name, ~r/^[a-z]*$/i)
    |> unique_constraint(:name, name: :unique_character_name)
    |> set_template_values
    |> clean_params
  end

  def clean_params(changeset) do
    changeset
    |> update_change(:name, &(String.trim(&1)))
    |> update_change(:name, &(String.replace(&1, " ", "")))
    |> update_change(:name, &(String.capitalize(&1)))
  end

  def set_template_values(changeset) do 
    case ServerRepo.get(RaceTemplate, get_field(changeset, :race)) do
      %RaceTemplate{} = race_template ->
        changeset
          |> put_change(:zone, race_template.zone)
          |> put_change(:map, race_template.map)
          |> put_change(:position_x, race_template.position_x)
          |> put_change(:position_y, race_template.position_y)
          |> put_change(:position_z, race_template.position_z)
          |> put_change(:orientation, race_template.orientation)
        _ ->
          changeset
          |> add_error(:race, "Race %{race} not found", [race: get_field(changeset, :race)])
    end
  end

  def update_character_count(account_id, realm_id) do
    character_count = ServerRepo.aggregate(characters_for_account_query(account_id), :count, :id)
    %RealmCharacterCount{account_id: account_id, realm_id: realm_id, character_count: character_count}
    |> LoginRepo.insert(on_conflict: :replace_all, conflict_target: [:account_id, :realm_id])
    character_count
  end

  def update_character_count_and_realm_population(account_id, :create) do
    realm_id = Application.get_env(:serverd, :realm_id, 1)
    character_count = update_character_count(account_id, realm_id)
    if character_count == 1 do # This is the first character created for the account
      from(r in Realm, where: r.id == ^realm_id)
      |> LoginRepo.update_all(inc: [population: 1])      
    end
  end

  def update_character_count_and_realm_population(account_id, :delete) do
    realm_id = Application.get_env(:serverd, :realm_id, 1)
    character_count = update_character_count(account_id, realm_id)
    if character_count == 0 do # Account has no more charactres on this realm
      from(r in Realm, where: r.id == ^realm_id)
      |> LoginRepo.update_all(inc: [population: -1])      
    end
  end
end