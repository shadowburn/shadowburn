defmodule Common.Character do
  use Ecto.Schema
  
  schema "character" do
    field :name, :string
    field :level, :integer

    field :account_id, :integer
    
    field :race, :integer
    field :class, :integer
    
    field :gender, :integer
    field :skin, :integer
    field :face, :integer
    field :hair_style, :integer
    field :hair_color, :integer
    field :facial_hair, :integer
      
    field :zone, :integer
    field :map, :integer
    field :position_x, :float
    field :position_y, :float
    field :position_z, :float
    field :orientation, :float

    field :hide_helm, :boolean
    field :hide_cloak, :boolean
    field :ghost, :boolean
    field :name_change, :boolean
    field :reset_spells, :boolean
    field :reset_talents, :boolean
    
    field :total_time, :integer

    field :is_deleted, :boolean

    timestamps()
  end
end