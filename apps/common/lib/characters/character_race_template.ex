defmodule Common.Character.RaceTemplate do
  use Ecto.Schema

  schema "character_race_template" do
    field :race, :integer

    field :zone, :integer
    field :map, :integer

    field :position_x, :float
    field :position_y, :float
    field :position_z, :float
    field :orientation, :float
  end
end