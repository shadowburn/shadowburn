defmodule Common.Character.Class do
  @classes %{
    warrior: 1,
    paladin: 2,
    hunter:  3,
    rogue:   4,
    priest:  5,
    shaman:  7,
    mage:    8,
    warlock: 9,
    druid:   11
  }

  # TODO: Macro this out.
end