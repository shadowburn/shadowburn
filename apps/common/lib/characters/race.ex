defmodule Common.Character.Race do
  @races %{
    human:    1,
    orc:      2,
    dwarf:    3,
    nightelf: 4,
    undead:   5,
    tauren:   6,
    gnome:    7,
    troll:    8
  }

  # TODO: Macro this out.
end