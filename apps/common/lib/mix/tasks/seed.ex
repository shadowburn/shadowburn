defmodule Mix.Tasks.Shadowburn.Seed do
  use Mix.Task

  @seed_files ["priv/login_repo/seeds.exs",
               "priv/server_repo/seeds.exs",
               "apps/common/priv/login_repo/seeds.exs",
               "apps/common/priv/server_repo/seeds.exs"]

  def run(_) do
    for file <- Enum.filter(@seed_files, &(File.exists?(&1))) do
      Mix.shell.cmd("mix run #{file}")      
    end
  end
end