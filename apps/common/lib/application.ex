defmodule Common.Application do
  use Application

  def start(_type, _args) do

    children = [
      # Start the Ecto repositories
      Common.LoginRepo,
      Common.ServerRepo
    ]

    opts = [strategy: :one_for_one, name: Common.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
