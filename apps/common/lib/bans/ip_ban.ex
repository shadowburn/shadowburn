defmodule Common.IpBan do
  use Ecto.Schema

  schema "ip_ban" do
    field :ip, :string

    field :ban_date, :utc_datetime_usec
    field :expiration_date, :utc_datetime_usec

    field :is_active, :boolean

    field :reason, :string

    timestamps()
  end
end
