defmodule Common.Bans do
  import Ecto.{Query, Changeset}

  require Logger

  alias Common.Account
  alias Common.AccountBan
  alias Common.IpBan
  alias Common.LoginRepo

  def ban(item, reason, duration \\ 0)

  def ban(%Account{} = account, reason, duration) do
    Logger.info "Banning #{account.username} for #{duration} seconds: #{reason}"
    %AccountBan{}
    |> new_ban_changeset(%{account: account, reason: reason, duration: duration})
    |> LoginRepo.insert()
  end

  def ban(ip, reason, duration) do
    Logger.info "Banning #{ip} for #{duration} seconds: #{reason}"
    %IpBan{}
    |> new_ban_changeset(%{ip: ip, reason: reason, duration: duration})
    |> LoginRepo.insert()
  end

  def is_banned(%Account{} = account) do
    query = from b in AccountBan,
            where: b.account_id == ^account.id and b.is_active == :true and (is_nil(b.expiration_date) or b.expiration_date > ^DateTime.utc_now),
            order_by: [desc: b.expiration_date],
            limit: 1

    ban = LoginRepo.all(query)

    case ban do
      [%AccountBan{expiration_date: nil}] -> {:banned, :permanent}
      [%AccountBan{}] -> {:banned, :temporary}
      [] -> :ok
    end
  end

  def is_banned(ip) do
    query = from b in IpBan,
            where: b.ip == ^ip and b.is_active == :true and (is_nil(b.expiration_date) or b.expiration_date > ^DateTime.utc_now),
            order_by: [desc: b.expiration_date],
            limit: 1

    ban = LoginRepo.all(query)

    case ban do
      [%IpBan{expiration_date: nil}] -> {:banned, :permanent}
      [%IpBan{}] -> {:banned, :temporary}
      [] -> :ok
    end
  end

  def check_auto_ban(account, current_ip) do
    with :true <- Application.get_env(:common, :auto_ban_enabled, :false),
         max_failed_logins <- Application.get_env(:common, :auto_ban_max_failed_logins, 5)
      do
        case account do
          %Account{failed_logins: failed_logins} when max_failed_logins > 0 and failed_logins >= max_failed_logins ->
            perform_auto_ban(account, current_ip, max_failed_logins)
          _ -> :true
        end
      end
  end

  defp perform_auto_ban(account, current_ip, max_failed_logins) do
    Logger.debug "Performing auto ban"
    with ban_type <- Application.get_env(:common, :auto_ban_type, :ip),
         ban_duration <- Application.get_env(:common, :auto_ban_duration_in_seconds, 300)
      do
        reason = "[AutoBan] #{max_failed_logins} failed logins, banned " <> if(ban_duration == 0, do: "permanently", else: "for #{ban_duration} seconds") <> "."
        case ban_type do
          :ip      -> ban(current_ip, reason, ban_duration)
          :account -> ban(account, reason, ban_duration)
          :account_and_ip ->
            ban(current_ip, reason, ban_duration)
            ban(account, reason, ban_duration)
        end
      end
  end

  defp new_ban_changeset(%IpBan{} = ip_ban, attrs) do
    ip_ban
    |> cast(attrs, [:ip, :reason])
    |> put_change(:ban_date, DateTime.utc_now)
    |> validate_required([:ip, :ban_date, :reason])
    |> set_ban_expiration(attrs.duration)
  end

  defp new_ban_changeset(%AccountBan{} = account_ban, attrs) do
    account_ban
    |> cast(attrs, [:reason])
    |> put_assoc(:account, attrs.account)
    |> put_change(:ban_date, DateTime.utc_now)
    |> validate_required([:account, :ban_date, :reason])
    |> set_ban_expiration(attrs.duration)
  end

  defp set_ban_expiration(changeset, duration) when is_integer(duration) do
    case duration do
      0    -> changeset #permanent
      _ ->
        ban_date = get_field(changeset, :ban_date)
        put_change(changeset, :expiration_date, DateTime.add(ban_date, duration, :second))
    end
  end
end
