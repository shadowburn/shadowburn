defmodule Common.AccountBan do
  use Ecto.Schema

  alias Common.Account

  schema "account_ban" do
    belongs_to :account, Account

    field :ban_date, :utc_datetime_usec
    field :expiration_date, :utc_datetime_usec

    field :is_active, :boolean

    field :reason, :string

    timestamps()
  end
end
