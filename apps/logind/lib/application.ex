defmodule Logind.Application do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do

    children = [
      {Task, fn -> Logind.accept(3724) end }
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Logind.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
