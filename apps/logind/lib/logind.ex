defmodule Logind do
  require Logger

  def accept(port) do
    {:ok, socket} = :gen_tcp.listen(port, [:binary, packet: :raw, active: true, reuseaddr: true])
    Logger.info "Logind listening on #{port}"
    loop_acceptor(socket)
  end

  defp loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    {:ok, pid} = Logind.Authenticator.start_link()
    :ok = :gen_tcp.controlling_process(client, pid)
    loop_acceptor(socket)
  end
end