defmodule Logind.Authenticator do
  @moduledoc """
  Handles talking back and forth with the client for authentication.

  Authentication for world of warcraft uses SRP6 and needs to follow a specific flow.
  For this reason, this is implemented as a `GenStateMachine` which will proceed to the next step 
  only if it's in the proper state to proceed.

  The process will start in the `:challenge` state. The goal is to get to the `:authed` state, which can be achieved by
  either the authentication flow, or reconnect flow.

  ### Authentication Flow
  The authentication flow starts with the receipt of the `:auth_challege` packet. If this step succeeds, the client is
  notified and the state is updated to `:auth_proof`. The `:auth_proof` packet then comes in and if successful the process
  moves to the `:authed` state.

      :challenege -> :auth_challenege -> :auth_proof -> :authed

  ### Reconnect Flow
  The reconnect flow starts with the receipt of the `:recon_challege` packet. If this step succeeds, the client is
  notified and the state is updated to `:recon_proof`. The `:recon_proof` packet then comes in and if successful the process
  moves to the `:authed` state.

      :challenege -> :recon_challenege -> :recon_proof -> :authed

  ### List Realms
  Once in the `:authed` state, the client can request a list of the realms.

  ### Errors
  If a packet is received out of order, the server will close the socket and the client will need to reconnect and start over.
  """

  use GenStateMachine

  require Logger

  import Binary, only: [reverse: 1]

  alias Common.Accounts
  alias Common.Bans
  alias Common.Realms

  # When the process is started, begin in the `:challenge` state, with an empty map of data.
  def init(_args), do: {:ok, :challenge, %{}}
  
  @doc """
  Starts the authenticator state machine.

  This is called in logind and immediately afterwards, the pid is set as the
  controlling process for the socket. Setting the controlling process ensures
  that incoming packets are directed to handle_event(:info, {:tcp, socket, packet}, state, data).

      {:ok, pid} = Logind.Authenticator.start_link()
      :ok = :gen_tcp.controlling_process(client, pid)
  """
  def start_link(), do: GenStateMachine.start_link(__MODULE__, nil)

  ##############
  # Public API #
  ##############

  @doc """
  The first step for initial SRP6 authentication.
  
  This will check to see that the account exists, whether the ip or account has
  been banned, and if the account is ip address locked (can only be accessed by
  the last ip on file.)
  
  In the event everything looks good for this account, the map returned will
  have the information required by the client to carry out the next step of
  SRP6 authentication.
  """
  def auth_challenge(username, current_ip, additional_data \\ %{}) do
    with :ok            <- Bans.is_banned(current_ip),
         {:ok, account} <- Accounts.get_account(username),
         :ok            <- Accounts.account_ip_locked(account, current_ip),
         :ok            <- Bans.is_banned(account)
      do
        {:ok, Map.merge(additional_data, Map.merge(%{username: username, current_ip: current_ip}, handle_logon_challenge(account)))}
      else
        {:error, reason}    -> {:error, reason}
        {:banned, ban_type} -> {:error, {:banned, ban_type}}
        :ip_locked          -> {:error, :ip_locked}
      end
  end

  @doc """
  The second and final step for initial SRP6 authentication.

  This will check the client proof to ensure that they have the correct password.

  If the proof is correct, we'll generate a server proof and send it back for
  the client to verify that we have the same session key.
  """
  def auth_proof(public_a, client_proof, additional_data \\ %{}) do
    case handle_logon_proof(additional_data, reverse(public_a), client_proof) do
      {:ok, state} -> 
        attrs = %{ session: Base.encode16(state.session),
                   os: state.os,
                   locale: state.locale,
                   last_ip: state.current_ip}
        Accounts.successful_login(state.account, attrs)
        Logger.info "[AuthProof] Successful Login for #{state.username}"
        {:ok, state}
      {:error, reason} -> 
        Accounts.failed_login(additional_data.account, additional_data.current_ip)
        Logger.info "[AuthProof] Invalid password for #{additional_data.username}: #{reason}"
        {:error, reason}
    end
  end

  @doc """
  The first step for a SRP6 reconnect.

  This will check to see that the account exists, whether the ip or account has
  been banned, and if the account is ip address locked (can only be accessed by
  the last ip on file.)

  In the event everything looks good for this account, the map returned will
  have the information required by the client to carry out the next step of
  the SRP6 reconnect process.
  """
  def recon_challenge(username, current_ip, additional_data \\ %{}) do
    with :ok             <- Bans.is_banned(current_ip),
          {:ok, account} <- Accounts.get_account(username),
          :ok            <- Accounts.account_ip_locked(account, current_ip),
          :ok            <- Bans.is_banned(account)
      do
        challenge_data = %{ challenge_data: :crypto.strong_rand_bytes(16),
                            session: Base.decode16!(account.session),
                            username: username,
                            account: account }
        {:ok, Map.merge(challenge_data, additional_data)}
      else
        {:error, reason}    -> {:error, reason}
        {:banned, ban_type} -> {:error, {:banned, ban_type}}
        :ip_locked          -> {:error, :ip_locked}
      end
  end

  @doc """
  The second and final step for a SRP6 reconnect.

  This will check the client proof to ensure that they have the correct session.

  If the proof is correct, we don't need to send any additional information, only
  an acknoledgement that it was correct.
  """
  def recon_proof(username, proof_data, client_proof, challenge_data, session), do: compare_reconnect_proof(username, proof_data, client_proof, challenge_data, session)

  @account_banned 3
  @account_unknown 4
  @account_suspended 12
  @account_ip_locked 12

  # Handles the `:auth_challege` packet that comes from the client.
  #
  # If no errors are returned from `auth_challenege/3`, we'll respond to the client
  # with the information required to carry out the next step of SRP6 authentication.
  #
  #   - `B` The public key for this authentication session
  #   - `g`
  #   - `n`
  #   - `salt` The user's salt
  #
  # Successful packet:
  #     <<command (0 - 1 byte), unknown (0 - 1 byte), error (0 - 1 byte),
  #       public_key (32 bytes), length of g (1 - 1 byte), g (in this case 1 byte),
  #       length of n (32 - 1 byte), n (in this case 32 bytes), salt (32 bytes),
  #       unknown (16 random bytes), security (0 for no additional security)>>
  #
  # Additional security would be two-factor authentication, which is currently not supported.
  def handle_event(:cast, {:auth_challenge, socket, packet}, :challenge, _data) do
    << _err                 :: little-size(8), 
       _size                :: little-size(16),
       _game                :: bytes-little-size(4),
       _v1                  :: little-size(8),
       _v2                  :: little-size(8),
       _v3                  :: little-size(8),
       _build               :: little-size(16),
       _platform            :: bytes-little-size(4),
       os                   :: bytes-size(4),
       locale               :: bytes-size(4),
       _utc_offset          :: little-size(32),
       _ip                  :: little-size(32),
       username_length      :: unsigned-little-size(8),
       username             :: bytes-little-size(username_length) >> = packet
    
    Logger.info "[AuthChallenge] Login for #{username}"

    {:ok, {ip_tuple, _port}} = :inet.peername(socket)
    current_ip = Enum.join(Tuple.to_list(ip_tuple), ".")
   
    additional_data = %{ os: String.reverse(String.trim(os, << 0 >>)),
                         locale: String.reverse(String.trim(locale, << 0 >>))}
    result = case auth_challenge(username, current_ip, additional_data) do
      {:ok, state} -> {:ok, state, << 0, 0, 0 >> <> reverse(state.public_b) <> << 1 >> <> state.g <> << 32 >> <> reverse(state.n) <> state.salt <> :crypto.strong_rand_bytes(16) <> << 0 >>}
      {:error, :account_unknown} -> {:error, << 1, 0, @account_unknown >>}
      {:error, :ip_locked} -> {:error, << 1, 0, @account_ip_locked >>}
      {:error, {:banned, :temporary}} -> {:error, << 1, 0, @account_suspended >>}
      {:error, {:banned, :permanent}} -> {:error, << 1, 0, @account_banned >>}
    end

    case result do
      {:ok, state, packet} ->
        :gen_tcp.send(socket, packet)
        {:next_state, :auth_proof, state}
      {:error, packet} ->
        :gen_tcp.send(socket, packet)
        exit(:normal)
    end
  end

  # Handles the `:auth_proof` packet that comes from the client.
  #
  # This will check to make sure that the password the client has is the correct password.
  # If the password is correct, we'll send back the server proof.
  #
  # Successful packet:
  #     <<command (1 - 1 byte), error (0 - 1 byte),
  #       server_proof (aka m2) (20 bytes), unknown (0 - 4 bytes)>>
  def handle_event(:cast, {:auth_proof, socket, packet}, :auth_proof, data) do
    << public_a        :: little-bytes-size(32),
       client_proof    :: little-bytes-size(20),
       _crc_hash       :: little-bytes-size(20),
       _num_keys       :: little-size(8),
       _security_flags :: little-size(8) >> = packet
  
    Logger.info "[AuthProof] Got client proof for #{data.username}"

    result = case auth_proof(public_a, client_proof, data) do
      {:ok, state} -> {:ok, state, << 1, 0 >> <> state.server_proof <> << 0, 0, 0, 0 >>}
      {:error, :invalid_password} -> {:error, << 1, 4 >>}
    end

    case result do
      {:ok, state, packet} ->
        :gen_tcp.send(socket, packet)
        {:next_state, :authed, %{account: state.account}}
      {:error, packet} ->
        :gen_tcp.send(socket, packet)
        exit(:normal)
    end
  end

  def handle_event(:cast, {:recon_challenge, socket, packet}, :challenge, _data) do
    << _err                 :: little-size(8), 
       _size                :: little-size(16),
       _game                :: bytes-little-size(4),
       _v1                  :: little-size(8),
       _v2                  :: little-size(8),
       _v3                  :: little-size(8),
       _build               :: little-size(16),
       _platform            :: bytes-little-size(4),
       _os                  :: bytes-little-size(4),
       _country             :: bytes-little-size(4),
       _utc_offset          :: little-size(32),
       _ip                  :: little-size(32),
       username_length      :: little-size(8),
       username             :: bytes-little-size(username_length) >> = packet    
    
    Logger.info "[ReconChallenge] Reconnect for #{username}"

    {:ok, {ip_tuple, _port}} = :inet.peername(socket)
    current_ip = Enum.join(Tuple.to_list(ip_tuple), ".")
  
    result = case recon_challenge(username, current_ip) do
      {:ok, state} -> {:ok, state, << 2, 0 >> <> state.challenge_data <> String.pad_leading("", 16, << 0 >>)}
      {:error, :account_unknown} -> {:error, << 2, @account_unknown >>}
      {:error, :ip_locked} -> {:error, << 2, @account_ip_locked >>}
      {:error, {:banned, :temporary}} -> {:error, << 2, @account_suspended >>}
      {:error, {:banned, :permanent}} -> {:error, << 2, @account_banned >>}
    end

    case result do
      {:ok, state, packet} ->
        :gen_tcp.send(socket, packet)
        {:next_state, :recon_proof, state}
      {:error, packet} ->
        :gen_tcp.send(socket, packet)
        exit(:normal)
    end
  end

  def handle_event(:cast, {:recon_proof, socket, packet}, :recon_proof, data) do
    << proof_data   :: little-bytes-size(16), 
       client_proof :: little-bytes-size(20),
       _crc_hash    :: little-bytes-size(20),
       _unknown     :: little-size(8)>> = packet
    
    Logger.info "[ReconProof] Reconnect proof for #{data.username}"
    
    case recon_proof(data.username, proof_data, client_proof, data.challenge_data, data.session) do
      :ok ->
        :gen_tcp.send(socket, << 3, 0 >>)
        {:next_state, :authed, %{account: data.account}}
      :error ->
        :gen_tcp.send(socket, << 3, 4 >>)
        exit(:normal)
    end
  end

  def handle_event(:cast, {:realm_list, socket, _rest}, :authed, data) do
    realms = Realms.get_realms_with_character_count(data.account.id)

    realm_packets = Enum.map(realms, fn({realm, character_count}) ->
      << realm.realm_type :: little-size(32) >> <> 
      << realm.flags :: size(8) >> <> 
      realm.name <> << 0 >> <>
      realm.address <> << 0 >> <>
      << realm.population / realm.max_population :: little-float-size(32) >> <> # population
      << character_count || 0>> <> # number of caracters
      << realm.timezone :: size(8) >> <>
      << 0 >> # unknown
    end)

    realm_list = Enum.reduce(realm_packets, << >>, fn(realm_packet, acc) -> acc <> realm_packet end)
    realm_count = Enum.count(realms)

    #               packet size                                   unknown     # of realms
    packet = << 16, byte_size(realm_list) + 7 :: little-size(16), 0, 0, 0, 0, realm_count >> <> realm_list <> << 2, 0 >>

    :gen_tcp.send(socket, packet)

    :keep_state_and_data
  end

  # Routes the packet to the correct handler based on the first byte of the incoming packet.
  # The remainder of the packet will be sent on (with the first byte stripped).
  # If the packet is not recognized, this process exits and the socket is closed.
  def handle_event(:info, {:tcp, socket, << packet_type :: unsigned-size(8), packet :: binary >>}, _state, _data) do
    case packet_type do
      0  -> GenStateMachine.cast(self(), {:auth_challenge, socket, packet})
      1  -> GenStateMachine.cast(self(), {:auth_proof, socket, packet})
      2  -> GenStateMachine.cast(self(), {:recon_challenge, socket, packet})
      3  -> GenStateMachine.cast(self(), {:recon_proof, socket, packet})
      16 -> GenStateMachine.cast(self(), {:realm_list, socket, packet})
      _  ->
        Logger.error("[Authenticator] Invalid packet type: #{packet_type}")
        exit(:normal)
    end
    :keep_state_and_data
  end

  # Handles the connection closing by exiting our process normally. Otherwise we'd see the process crash because we're not handling `:tcp_closed`.
  def handle_event(:info, {:tcp_closed, _socket}, _state, _data) do
    Logger.debug("[Authenticator] Client has disconnected")
    exit(:normal)
  end

  # Handles any incoming packet that comes in while the GenServer is not in the appropriate state to process it.
  # Examples:
  # - A `:recon_proof` packet comes in while we're in `:challenge`
  # - A second `:auth_challenge` packet comes in while we're in `:auth_proof` from the first one
  def handle_event(:cast, {event_type, _socket, _rest}, current_state, _data) do
    Logger.debug("[Authenticator] Invalid event_type: #{event_type} for current_state: #{current_state}")
    exit(:normal)
  end

  ##################
  # SRP6 Functions #
  ##################
  @n << 137, 75, 100, 94, 137, 225, 83, 91, 189, 173, 91, 139, 41, 6, 80, 83, 8, 1, 177, 142, 191, 191, 94, 143, 171, 60, 130, 135, 42, 62, 155, 183 >>
  @g << 7 >>
  @k << 3 >>

  defp default_state_for_account(account) do
    %{n: @n, g: @g, k: @k}
    |> Map.merge(%{account: account})
    |> Map.merge(%{username: account.username})
    |> Map.merge(%{salt: Base.decode16!(account.salt)})
    |> Map.merge(%{verifier: Base.decode16!(account.verifier)})
  end

  defp handle_logon_challenge(account) do
    default_state_for_account(account)
    |> calculate_b()
    |> calculate_B()
  end

  defp calculate_b(state) do
    private_b = :crypto.strong_rand_bytes(19)
    Map.merge(state, %{private_b: private_b})
  end

  defp calculate_B(state) do
    {public_b, _} = :crypto.generate_key(:srp, {:host, [state.verifier, state.g, state.n, :"6"]}, state.private_b)
    Map.merge(state, %{public_b: public_b})
  end

  defp handle_logon_proof(state, public_a, client_proof) do
    public_a_reversed = reverse(public_a)

    scrambler = :crypto.hash(:sha, public_a_reversed <> reverse(state.public_b))
    s = reverse(:crypto.compute_key(:srp, public_a, {state.public_b, state.private_b}, {:host, [state.verifier, state.n, :"6", reverse(scrambler)]}))
   
    session = interleave(s)
    
    mod_hash = :crypto.hash(:sha, reverse(state.n))
    generator_hash = :crypto.hash(:sha, state.g)
    t3 = :crypto.exor(mod_hash, generator_hash)
    t4 = :crypto.hash(:sha, state.username)
    m = :crypto.hash(:sha, t3 <> t4 <> state.salt <> public_a_reversed <> reverse(state.public_b) <> session)
    
    if m == client_proof do
      server_proof = compute_server_proof(public_a, client_proof, session)
      {:ok, Map.merge(state, %{ public_a: public_a, session: session, server_proof: server_proof })}
    else
      {:error, :invalid_password }
    end
  end

  defp compute_server_proof(public_a, client_proof, session) do
    :crypto.hash(:sha, reverse(public_a) <> client_proof <> session)
  end

  defp interleave(s) do
    list = Binary.to_list(s)

    t1 = Binary.from_list(interleave_t1(list))
    t2 = Binary.from_list(interleave_t2(list))

    t1_hash = Binary.to_list(:crypto.hash(:sha, t1))
    t2_hash = Binary.to_list(:crypto.hash(:sha, t2))

    Binary.from_list(List.flatten(Enum.map(List.zip([t1_hash, t2_hash]), &Tuple.to_list/1)))
  end

  defp interleave_t1([a, _ | rest]), do: [a | interleave_t1(rest)]
  defp interleave_t1([]), do: []
  
  defp interleave_t2([_, b | rest]), do: [b | interleave_t2(rest)]
  defp interleave_t2([]), do: []

  defp compare_reconnect_proof(username, proof_data, client_proof, challenge_data, session) do
    data = username <> proof_data <> challenge_data <> session
    if(:crypto.hash(:sha, data) == client_proof, do: :ok, else: :error)
  end
end