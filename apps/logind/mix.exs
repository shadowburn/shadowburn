defmodule Logind.Mixfile do
  use Mix.Project

  def project do
    [app: :logind,
     version: "0.1.0",
     build_path: "../../_build",
     config_path: "../../config/config.exs",
     deps_path: "../../deps",
     lockfile: "../../mix.lock",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: [ test: "test --no-start" ],
     deps: deps(),
     #Docs
     name: "Shadowburn logind",
     source_url: "https://gitlab.com/shadowburn/shadowburn",
     homepage_url: "https://gitlab.com/shadowburn/shadowburn",
     docs: [main: "Shadowburn",
            extras: ["README.md"]]
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [mod: {Logind.Application, []},
      extra_applications: [:logger, :runtime_tools]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:my_dep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:my_dep, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
  #
  # To depend on another app inside the umbrella:
  #
  #   {:my_app, in_umbrella: true}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [{:common, in_umbrella: true},
     {:binary, "~> 0.0.4"},
     {:gen_state_machine, "~> 2.0"}]
  end
end
