defmodule SessionTest do
  use ExUnit.Case

  alias Serverd.Session
  alias Common.Account

  describe "when checking user's auth" do
    test "should pass if digest is correct" do
      account = %Account{username: "test", session: "AB"}
      server_seed = <<1, 2, 3, 4>>
      client_seed = <<4, 3, 2, 1>>
      digest = :crypto.hash(:sha, account.username <> << 0 :: little-size(32) >> <> client_seed <> server_seed <> Base.decode16!(account.session))
      assert :ok = Session.check_auth(account, server_seed, client_seed, digest)
    end

    test "should fail if digest is not correct" do
      account = %Account{username: "test", session: "AB"}
      server_seed = <<1, 2, 3, 4>>
      client_seed = <<4, 3, 2, 1>>
      digest = :crypto.hash(:sha, << 0, 1, 2 >>)
      assert {:error, :invalid_digest} = Session.check_auth(account, server_seed, client_seed, digest)
    end
  end
end