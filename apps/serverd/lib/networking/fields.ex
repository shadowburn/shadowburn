defmodule Fields do
  @object_fields %{
    :id      => {0x00, :int64},
    :type    => {0x02, :int32},
    :entry   => {0x03, :int32},
    :scale_x => {0x04, :float},

    # UNIT FIELDS
    :charm         => {0x06 + 0x00, :int64},
    :summon        => {0x06 + 0x02, :int64},
    :charmed_by    => {0x06 + 0x04, :int64},
    :summoned_by   => {0x06 + 0x06, :int64},
    :created_by    => {0x06 + 0x08, :int64},
    :target        => {0x06 + 0x0A, :int64},
    :pursuaded     => {0x06 + 0x0C, :int64},
    :channel_obect => {0x06 + 0x0E, :int64},

    :health  => {0x06 + 0x10, :int32},
    :power_1 => {0x06 + 0x11, :int32},
    :power_2 => {0x06 + 0x12, :int32},
    :power_3 => {0x06 + 0x13, :int32},
    :power_4 => {0x06 + 0x14, :int32},
    :power_5 => {0x06 + 0x15, :int32},

    :max_health  => {0x06 + 0x16, :int32},
    :max_power_1 => {0x06 + 0x17, :int32},
    :max_power_2 => {0x06 + 0x18, :int32},
    :max_power_3 => {0x06 + 0x19, :int32},
    :max_power_4 => {0x06 + 0x1A, :int32},
    :max_power_5 => {0x06 + 0x1B, :int32},

    :level                        => {0x06 + 0x1C, :int32},
    :faction_template             => {0x06 + 0x1D, :int32},
    :unit_bytes_0                 => {0x06 + 0x1E, :four_bytes},
    :virtual_item_slot_display    => {0x06 + 0x1F, :int32},
    :virtual_item_slot_display_01 => {0x06 + 0x20, :int32},
    :virtual_item_slot_display_02 => {0x06 + 0x21, :int32},
    :virtual_item_info            => {0x06 + 0x22, :four_bytes},
    :virtual_item_info_01         => {0x06 + 0x23, :four_bytes},
    :virtual_item_info_02         => {0x06 + 0x24, :four_bytes},
    :virtual_item_info_03         => {0x06 + 0x25, :four_bytes},
    :virtual_item_info_04         => {0x06 + 0x26, :four_bytes},
    :virtual_item_info_05         => {0x06 + 0x27, :four_bytes},
    :unit_flags                   => {0x06 + 0x28, :int32},

    :aura                   => {0x06 + 0x29, :int32},
    :aura_last              => {0x06 + 0x58, :int32},
    :aura_flags             => {0x06 + 0x59, :int32},
    :aura_flags_01          => {0x06 + 0x5A, :int32},
    :aura_flags_02          => {0x06 + 0x5B, :int32},
    :aura_flags_03          => {0x06 + 0x5C, :int32},
    :aura_flags_04          => {0x06 + 0x5D, :int32},
    :aura_flags_05          => {0x06 + 0x5E, :int32},
    :aura_levels            => {0x06 + 0x5F, :four_bytes},
    :aura_levels_last       => {0x06 + 0x6A, :int32},
    :aura_applications      => {0x06 + 0x6B, :four_bytes},
    :aura_applications_last => {0x06 + 0x76, :int32},
    :aura_state             => {0x06 + 0x77, :int32},

    :base_attack_time    => {0x06 + 0x78, :int32},
    :offhand_attack_time => {0x06 + 0x79, :int32},
    :ranged_attack_time  => {0x06 + 0x7A, :int32},

    :bounding_radius => {0x06 + 0x7B, :float},
    :combat_reach    => {0x06 + 0x7C, :float},

    :display_id        => {0x06 + 0x7D, :int32},
    :native_display_id => {0x06 + 0x7E, :int32},
    :mount_display_id  => {0x06 + 0x7F, :int32},

    :min_damage         => {0x06 + 0x80, :float},
    :max_damage         => {0x06 + 0x81, :float},
    :min_offhand_damage => {0x06 + 0x82, :float},
    :max_offhand_damage => {0x06 + 0x83, :float},

    :unit_bytes_1 => {0x06 + 0x84, :four_bytes},

    :pet_number                => {0x06 + 0x85, :int32},
    :pet_name_timestamp        => {0x06 + 0x86, :int32},
    :pet_exerience             => {0x06 + 0x87, :int32},
    :pet_next_level_experience => {0x06 + 0x88, :int32},

    :dynamic_flags    => {0x06 + 0x89, :int32},
    :channel_spell    => {0x06 + 0x8A, :int32},
    :mod_cast_speed   => {0x06 + 0x8B, :float},
    :created_by_spell => {0x06 + 0x8C, :int32},

    :npc_flags       => {0x06 + 0x8D, :int32},
    :npc_emote_state => {0x06 + 0x8E, :int32},

    :training_points => {0x06 + 0x8F, :two_int16},

    :strength => {0x06 + 0x90, :int32},
    :agility => {0x06 + 0x91, :int32},
    :stamina => {0x06 + 0x92, :int32},
    :intellect => {0x06 + 0x93, :int32},
    :spirit => {0x06 + 0x94, :int32},

    :armor => {0x06 + 0x95, :int32},
    :resistance_1 => {0x06 + 0x96, :int32},
    :resist_fire => {0x06 + 0x97, :int32},
    :resist_nature => {0x06 + 0x98, :int32},
    :resist_frost => {0x06 + 0x99, :int32},
    :resist_shadow => {0x06 + 0x9A, :int32},
    :resist_arcane => {0x06 + 0x9B, :int32},

    :base_mana   => {0x06 + 0x9C, :int32},
    :base_health => {0x06 + 0x9D, :int32},

    :unit_bytes_2 => {0x06 + 0x9E, :four_bytes},

    :attack_power            => {0x06 + 0x9F, :int32},
    :attack_power_mods       => {0x06 + 0xA0, :two_int16},
    :attack_power_multiplier => {0x06 + 0xA1, :float},

    :ranged_attack_power            => {0x06 + 0xA2, :int32},
    :ranged_attack_power_mods       => {0x06 + 0xA3, :two_int16},
    :ranged_attack_power_multiplier => {0x06 + 0xA4, :float},

    :min_ranged_damage        => {0x06 + 0xA5, :float},
    :max_ranged_damage        => {0x06 + 0xA6, :float},

    :power_cost_modifier_0 => {0x06 + 0xA7, :int32},
    :power_cost_modifier_1 => {0x06 + 0xA8, :int32},
    :power_cost_modifier_2 => {0x06 + 0xA9, :int32},
    :power_cost_modifier_3 => {0x06 + 0xAA, :int32},
    :power_cost_modifier_4 => {0x06 + 0xAB, :int32},
    :power_cost_modifier_5 => {0x06 + 0xAC, :int32},
    :power_cost_modifier_6 => {0x06 + 0xAD, :int32},

    :power_cost_multiplier_0 => {0x06 + 0xAE, :float},
    :power_cost_multiplier_1 => {0x06 + 0xAF, :float},
    :power_cost_multiplier_2 => {0x06 + 0xB0, :float},
    :power_cost_multiplier_3 => {0x06 + 0xB1, :float},
    :power_cost_multiplier_4 => {0x06 + 0xB2, :float},
    :power_cost_multiplier_5 => {0x06 + 0xB3, :float},
    :power_cost_multiplier_6 => {0x06 + 0xB4, :float},

    # PLAYER FIELDS
    :duel_arbiter => {0xB6 + 0x00, :int64},
    :player_flags => {0xB6 + 0x02, :int32},

    :guild_id   => {0xB6 + 0x03, :int32},
    :guild_rank => {0xB6 + 0x04, :int32},

    :player_bytes_1 => {0xB6 + 0x05, :four_bytes},
    :player_bytes_2 => {0xB6 + 0x06, :four_bytes},
    :player_bytes_3 => {0xB6 + 0x07, :four_bytes},

    :duel_team       => {0xB6 + 0x08, :int32},
    :guild_timestamp => {0xB6 + 0x09, :int32},

    :quest_log_1_1 => {0xB6 + 0x0A, :int32},
    :quest_log_1_2 => {0xB6 + 0x0B, :int32},
    :quest_log_1_3 => {0xB6 + 0x0C, :int32},

    :quest_log_last_1 => {0xB6 + 0x43, :int32},
    :quest_log_last_2 => {0xB6 + 0x44, :int32},
    :quest_log_last_3 => {0xB6 + 0x45, :int32},

    :visible_item_1_creator    => {0xB6 + 0x46, :int64},
    :visible_item_1_0          => {0xB6 + 0x48, :int64},
    :visible_item_1_properties => {0xB6 + 0x50, :int32},

    :visible_item_last_creator    => {0xB6 + 0x011E, :int64},
    :visible_item_last_0          => {0xB6 + 0x0120, :int64},
    :visible_item_last_properties => {0xB6 + 0x0128, :int32},

    :inv_slot_head => {0xB6 + 0x012A, :int64},

    :pack_slot_1    => {0xB6 + 0x0158, :int64},
    :pack_slot_last => {0xB6 + 0x0176, :int64},

    :bank_slot_1    => {0xB6 + 0x0178, :int64},
    :bank_slot_last => {0xB6 + 0x01A6, :int64},

    :bank_bag_slot_1    => {0xB6 + 0x01A8, :int64},
    :bank_bag_slot_last => {0xB6 + 0x0AB2, :int64},

    :vendor_buy_back_slot_1    => {0xB6 + 0x01B4, :int64},
    :vendor_buy_back_slot_last => {0xB6 + 0x01CA, :int64},

    :keyring_slot_1    => {0xB6 + 0x01CC, :int64},
    :keyring_slot_last => {0xB6 + 0x020A, :int64},

    :far_sight    => {0xB6 + 0x020C, :int64},
    :combo_target => {0xB6 + 0x020E, :int64},

    :experience            => {0xB6 + 0x0210, :int32},
    :next_level_experience => {0xB6 + 0x0211, :int32},

    :skill_info_1_id         => {0xB6 + 0x0212, :int32}, # 128 Skills
    :skill_info_1_level      => {0xB6 + 0x0213, :int32}, # 3 entries per skill
    :skill_info_1_stat_level => {0xB6 + 0x0214, :int32},

    :character_points_1 => {0xB6 + 0x0392, :int32},
    :character_points_2 => {0xB6 + 0x0393, :int32},

    :track_creatures => {0xB6 + 0x0394, :int32},
    :track_resources => {0xB6 + 0x0395, :int32},

    :block_percentage       => {0xB6 + 0x0396, :float},
    :dodge_percentage       => {0xB6 + 0x0397, :float},
    :parry_percentage       => {0xB6 + 0x0398, :float},
    :crit_percentage        => {0xB6 + 0x0399, :float},
    :ranged_crit_percentage => {0xB6 + 0x039A, :float},

    :explored_zones_1 => {0xB6 + 0x039B, :four_bytes}, # 32 zones

    :rest_state_exp => {0xB6 + 0x03DB, :int32},

    :coinage => {0xB6 + 0x03DC, :int32},

    :pos_stat_0 => {0xB6 + 0x03DD, :int32},
    :pos_stat_1 => {0xB6 + 0x03DE, :int32},
    :pos_stat_2 => {0xB6 + 0x03DF, :int32},
    :pos_stat_3 => {0xB6 + 0x03E0, :int32},
    :pos_stat_4 => {0xB6 + 0x03E1, :int32},
    :neg_stat_0 => {0xB6 + 0x03E2, :int32},
    :neg_stat_1 => {0xB6 + 0x03E3, :int32},
    :neg_stat_2 => {0xB6 + 0x03E4, :int32},
    :neg_stat_3 => {0xB6 + 0x03E5, :int32},
    :neg_stat_4 => {0xB6 + 0x03E6, :int32},

    :resistance_0_buff_mod_pos => {0xB6 + 0x03E7, :int32},
    :resistance_1_buff_mod_pos => {0xB6 + 0x03E8, :int32},
    :resistance_2_buff_mod_pos => {0xB6 + 0x03E9, :int32},
    :resistance_3_buff_mod_pos => {0xB6 + 0x03EA, :int32},
    :resistance_4_buff_mod_pos => {0xB6 + 0x03EB, :int32},
    :resistance_5_buff_mod_pos => {0xB6 + 0x03EC, :int32},
    :resistance_6_buff_mod_pos => {0xB6 + 0x03ED, :int32},
    :resistance_0_buff_mod_neg => {0xB6 + 0x03EE, :int32},
    :resistance_1_buff_mod_neg => {0xB6 + 0x03EF, :int32},
    :resistance_2_buff_mod_neg => {0xB6 + 0x03F0, :int32},
    :resistance_3_buff_mod_neg => {0xB6 + 0x03F1, :int32},
    :resistance_4_buff_mod_neg => {0xB6 + 0x03F2, :int32},
    :resistance_5_buff_mod_neg => {0xB6 + 0x03F3, :int32},
    :resistance_6_buff_mod_neg => {0xB6 + 0x03F4, :int32},

    :mod_damage_0_done_pos => {0xB6 + 0x03F5, :int32},
    :mod_damage_1_done_pos => {0xB6 + 0x03F6, :int32},
    :mod_damage_2_done_pos => {0xB6 + 0x03F7, :int32},
    :mod_damage_3_done_pos => {0xB6 + 0x03F8, :int32},
    :mod_damage_4_done_pos => {0xB6 + 0x03F9, :int32},
    :mod_damage_5_done_pos => {0xB6 + 0x03FA, :int32},
    :mod_damage_6_done_pos => {0xB6 + 0x03FB, :int32},
    :mod_damage_0_done_neg => {0xB6 + 0x03FC, :int32},
    :mod_damage_1_done_neg => {0xB6 + 0x03FD, :int32},
    :mod_damage_2_done_neg => {0xB6 + 0x03FE, :int32},
    :mod_damage_3_done_neg => {0xB6 + 0x03FF, :int32},
    :mod_damage_4_done_neg => {0xB6 + 0x0400, :int32},
    :mod_damage_5_done_neg => {0xB6 + 0x0401, :int32},
    :mod_damage_6_done_neg => {0xB6 + 0x0402, :int32},
    :mod_damage_0_done_pct => {0xB6 + 0x0403, :float},
    :mod_damage_1_done_pct => {0xB6 + 0x0404, :float},
    :mod_damage_2_done_pct => {0xB6 + 0x0405, :float},
    :mod_damage_3_done_pct => {0xB6 + 0x0406, :float},
    :mod_damage_4_done_pct => {0xB6 + 0x0407, :float},
    :mod_damage_5_done_pct => {0xB6 + 0x0408, :float},
    :mod_damage_6_done_pct => {0xB6 + 0x0409, :float},

    :player_bytes_4 => {0xB6 + 0x040A, :four_bytes},

    :ammo_id => {0xB6 + 0x040B, :int32},

    :self_res_spell => {0xB6 + 0x040C, :int32},

    :pvp_medals => {0xB6 + 0x040D, :int32},

    :buyback_price_1    => {0xB6 + 0x040E, :int32},
    :buyback_price_last => {0xB6 + 0x0419, :int32},

    :buyback_timestamp_1    => {0xB6 + 0x041A, :int32},
    :buyback_timestamp_last => {0xB6 + 0x0425, :int32},

    :session_kills          => {0xB6 + 0x0426, :int32},
    :yesterday_kills        => {0xB6 + 0x0427, :int32},
    :last_week_kills        => {0xB6 + 0x0428, :int32},
    :this_week_kills        => {0xB6 + 0x0429, :int32},
    :this_week_contribution => {0xB6 + 0x042A, :int32},
    :honorable_kills        => {0xB6 + 0x042B, :int32},
    :dishonorable_kills     => {0xB6 + 0x042C, :int32},
    :yesterday_contribution => {0xB6 + 0x042D, :int32},
    :last_week_contribution => {0xB6 + 0x042E, :int32},
    :last_week_rank         => {0xB6 + 0x042F, :int32},

    :player_bytes_5 => {0xB6 + 0x0430, :four_bytes},

    :watched_faction_index => {0xB6 + 0x0431, :int32},
    :combat_rating_1 => {0xB6 + 0x0432, :int32}
  }

  def object_field(field), do: Map.get(@object_fields, field)
end
