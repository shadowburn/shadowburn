defmodule Serverd.Application do
  use Application

  def start(_type, _args) do
    children = [
      {Task, fn -> Serverd.accept(8085) end}
    ]

    opts = [strategy: :one_for_one, name: Serverd.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
