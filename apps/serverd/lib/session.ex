defmodule Serverd.Session do
  use GenServer
  use Bitwise

  require Logger

  alias Common.Accounts
  alias Common.Bans
  alias Common.Character
  alias Common.Characters
  alias Fields

  import Binary, only: [split_at: 2, trim_trailing: 1, pad_trailing: 2]

  @addon_public_key << 0xC3, 0x5B, 0x50, 0x84, 0xB9, 0x3E, 0x32, 0x42, 0x8C, 0xD0, 0xC7, 0x48, 0xFA, 0x0E, 0x5D, 0x54, 0x5A, 0xA3, 0x0E, 0x14, 0xBA, 0x9E, 0x0D, 0xB9, 0x5D, 0x8B, 0xEE, 0xB6, 0x84, 0x93, 0x45, 0x75, 0xFF, 0x31, 0xFE, 0x2F, 0x64, 0x3F, 0x3D, 0x6D, 0x07, 0xD9, 0x44, 0x9B, 0x40, 0x85, 0x59, 0x34, 0x4E, 0x10, 0xE1, 0xE7, 0x43, 0x69, 0xEF, 0x7C, 0x16, 0xFC, 0xB4, 0xED, 0x1B, 0x95, 0x28, 0xA8, 0x23, 0x76, 0x51, 0x31, 0x57, 0x30, 0x2B, 0x79, 0x08, 0x50, 0x10, 0x1C, 0x4A, 0x1A, 0x2C, 0xC8, 0x8B, 0x8F, 0x05, 0x2D, 0x22, 0x3D, 0xDB, 0x5A, 0x24, 0x7A, 0x0F, 0x13, 0x50, 0x37, 0x8F, 0x5A, 0xCC, 0x9E, 0x04, 0x44, 0x0E, 0x87, 0x01, 0xD4, 0xA3, 0x15, 0x94, 0x16, 0x34, 0xC6, 0xC2, 0xC3, 0xFB, 0x49, 0xFE, 0xE1, 0xF9, 0xDA, 0x8C, 0x50, 0x3C, 0xBE, 0x2C, 0xBB, 0x57, 0xED, 0x46, 0xB9, 0xAD, 0x8B, 0xC6, 0xDF, 0x0E, 0xD6, 0x0F, 0xBE, 0x80, 0xB3, 0x8B, 0x1E, 0x77, 0xCF, 0xAD, 0x22, 0xCF, 0xB7, 0x4B, 0xCF, 0xFB, 0xF0, 0x6B, 0x11, 0x45, 0x2D, 0x7A, 0x81, 0x18, 0xF2, 0x92, 0x7E, 0x98, 0x56, 0x5D, 0x5E, 0x69, 0x72, 0x0A, 0x0D, 0x03, 0x0A, 0x85, 0xA2, 0x85, 0x9C, 0xCB, 0xFB, 0x56, 0x6E, 0x8F, 0x44, 0xBB, 0x8F, 0x02, 0x22, 0x68, 0x63, 0x97, 0xBC, 0x85, 0xBA, 0xA8, 0xF7, 0xB5, 0x40, 0x68, 0x3C, 0x77, 0x86, 0x6F, 0x4B, 0xD7, 0x88, 0xCA, 0x8A, 0xD7, 0xCE, 0x36, 0xF0, 0x45, 0x6E, 0xD5, 0x64, 0x79, 0x0F, 0x17, 0xFC, 0x64, 0xDD, 0x10, 0x6F, 0xF3, 0xF5, 0xE0, 0xA6, 0xC3, 0xFB, 0x1B, 0x8C, 0x29, 0xEF, 0x8E, 0xE5, 0x34, 0xCB, 0xD1, 0x2A, 0xCE, 0x79, 0xC3, 0x9A, 0x0D, 0x36, 0xEA, 0x01, 0xE0, 0xAA, 0x91, 0x20, 0x54, 0xF0, 0x72, 0xD8,0x1E, 0xC7, 0x89, 0xD2 >>

  def start_link(socket), do: GenServer.start_link(__MODULE__, %{socket: socket})

  def init(state) do
    GenServer.cast(self(), :smsg_send_auth)

    {:ok, {ip_tuple, _port}} = :inet.peername(state.socket)
    current_ip = Enum.join(Tuple.to_list(ip_tuple), ".")

    seed = :crypto.strong_rand_bytes(4)
    crypt = %{initialized: :false}
    {:ok, Map.merge(state, %{seed: seed, crypt: crypt, current_ip: current_ip, last_ping: 0, latency: 0, packet_stream: <<>>})}
  end

  def send_packet(op_code, payload) when is_integer(payload), do: send_packet(op_code, << payload >>)
  def send_packet(op_code, payload) when is_binary(payload) do
    GenServer.cast(self(), {:send_packet, {:ok, op_code, payload}})
  end

  def send_error_packet(op_code, payload) when is_integer(payload), do: send_error_packet(op_code, << payload >>)
  def send_error_packet(op_code, payload) when is_binary(payload) do
    GenServer.cast(self(), {:send_packet, {:error, op_code, payload}})
  end

  def init_encryption(key) do
    GenServer.cast(self(), {:init_encryption, key})
  end

  def set_account(account) do
    GenServer.cast(self(), {:set_account, account})
  end

  def set_character(character) do
    GenServer.cast(self(), {:set_character, character})
  end

  def set_latency(latency, last_ping) do
    GenServer.cast(self(), {:set_latency, latency, last_ping})
  end

  def encrypt_header(header, state) do
    case state.initialized do
      :true ->
        initial_acc = {<<>>, %{send_i: state.send_i, send_j: state.send_j}}
        {header, crypt_state} = Enum.reduce(:binary.bin_to_list(header),
                                            initial_acc,
                                            fn(byte, {header, crypt}) ->
                                              send_i = rem(crypt.send_i, byte_size(state.key))
                                              x = bxor(byte, :binary.at(state.key, send_i)) + crypt.send_j
                                              << truncated_x >> = << x :: little-size(8) >>
                                              {header <> << truncated_x >>, %{send_i: send_i + 1, send_j: truncated_x }}
                                            end)
        {header, Map.merge(state, crypt_state)}
      _ -> {header, state}
    end
  end

  def decrypt_header(header, state) do
    case state.initialized do
      :true ->
        initial_acc = {<<>>, %{recv_i: state.recv_i, recv_j: state.recv_j}}
        {header, crypt_state} = Enum.reduce(:binary.bin_to_list(header),
                                            initial_acc,
                                            fn(byte, {header, crypt}) ->
                                              recv_i = rem(crypt.recv_i, byte_size(state.key))
                                              x = bxor(byte - crypt.recv_j, :binary.at(state.key, recv_i))
                                              << truncated_x >> = << x :: little-size(8) >>
                                              {header <> << truncated_x >>, %{recv_i: recv_i + 1, recv_j: byte }}
                                            end)
        {header, Map.merge(state, crypt_state)}
      _ -> {header, state}
    end
  end

  def handle_ping(payload, _state) do
    << ping    :: little-size(32),
       latency :: little-size(32) >> = payload

    send_packet(0x1DD, << ping :: little-size(32) >>)
    set_latency(latency, :os.system_time)
  end

  def handle_auth(payload, state) do
    << _build  :: little-size(32),
       _server :: little-size(32),
       payload :: binary >> = payload

    {:ok, username, payload} = parse_string(payload)

    << client_seed :: little-bytes-size(4),
       digest      :: little-bytes-size(20),
       _payload     :: binary >> = payload

    with :ok            <- Bans.is_banned(state.current_ip),
         {:ok, account} <- Accounts.get_account(username),
         :ok            <- Accounts.account_ip_locked(account, state.current_ip),
         :ok            <- Bans.is_banned(account),
         :ok            <- check_auth(account, state.seed, client_seed, digest)
      do
        set_account(account)
        init_encryption(Base.decode16!(account.session))
        # handle_addon(payload)
        send_packet(0x1EE, << 0x0C, 0 :: little-size(32), 0, 0 :: little-size(32) >>)
      else
        :ip_locked                 -> send_error_packet(0x1EE, 0x0D)
        {:banned, _type}           -> send_error_packet(0x1EE, 0x1C)
        {:error, :account_unknown} -> send_error_packet(0x1EE, 0x15)
        {:error, :invalid_digest}  -> send_error_packet(0x1EE, 0x0D)
      end
  end

  def check_auth(account, server_seed, client_seed, digest) do
    data = account.username <> << 0 :: little-size(32) >> <> client_seed <> server_seed <> Base.decode16!(account.session)
    if(:crypto.hash(:sha, data) == digest, do: :ok, else: {:error, :invalid_digest})
  end

  def handle_addon(payload) do
    << _size :: little-size(32),
       zip   :: binary >> = payload

    uncompressed = :zlib.uncompress(zip)
    addons = get_addons(uncompressed)
    # addon_packets = for a <- addons, do: << 2 :: little-size(32), 1 :: little-size(32) >> <> public_key(a) <> << 0, 0 :: little-size(32), 0, 0 :: little-size(32) >>
    addon_packets = for _ <- addons, do: << 2 :: little-size(8), 1 :: little-size(8), 0, 0, 0, 0, 0, 0 >>
    addon_packet = Enum.join(addon_packets)
    send_packet(0x2EF, addon_packet)
  end

  def public_key(addon) do
    case addon do
      0x4c1c776d -> << 0 >>
      _          -> << 1 >> <> @addon_public_key
    end
  end

  def get_addons(payload, addons \\ [])
  def get_addons(<<>>, addons), do: addons
  def get_addons(payload, addons) do
    {:ok, name, payload} = parse_string(payload)
    << crc  :: little-size(32),
       unk7 :: little-size(32),
       unk8 :: little-size(8),
       payload :: binary >> = payload
    get_addons(payload, addons ++ [%{name: name, crc: crc, unk7: unk7, unk8: unk8}])
  end

  def parse_string(payload, pos \\ 1)
  def parse_string(payload, _pos) when byte_size(payload) == 0, do: {:ok, payload, <<>>}
  def parse_string(payload, pos) do
    case :binary.at(payload, pos - 1) do
      0 ->
        {string, rest} = split_at(payload, pos)
        {:ok, trim_trailing(string), rest}
      _ -> parse_string(payload, pos + 1)
    end
  end

  def handle_char_enum(<<>>, state) do
    characters = Characters.get_characters_for_account(state.account.id)
    payload = << length(characters) >> <> Enum.join(for c <- characters do
      char_flags = if c.name_change, do: 16384, else: 0
      << c.id :: little-size(64) >> <> # guid
         c.name <> << 0, # name
         c.race, # race
         c.class, # class
         c.gender, # gender
         c.skin, # skin
         c.face, # face
         c.hair_style, # hair style
         c.hair_color, # hair color
         c.facial_hair, # facial hair
         c.level, # level
         c.zone :: little-size(32), # zone
         c.map :: little-size(32), # map
         c.position_x :: float-size(32), # x
         c.position_y :: float-size(32), # y
         c.position_z :: float-size(32), # z
         0 :: little-size(32), # guild id
         char_flags :: little-size(32), # character flags
         0, # first login
         0 :: little-size(32), # pet id
         0 :: little-size(32), # pet level
         0 :: little-size(32), # pet family
         0 :: little-size(760), # equipment
         0 :: little-size(32), # first bag display id?
         0 >> # first bag inventory type?
    end)
    send_packet(0x3b, payload)
  end

  def handle_char_create(payload, state) do
    {:ok, character_name, payload} = parse_string(payload)

    << race, class, gender, skin, face, hair_style, hair_color, facial_hair, _outfit_id >> = payload

    case Characters.create_character(state.account.id, character_name, race, class, gender, skin, face, hair_style, hair_color, facial_hair) do
      {:ok, _} -> send_packet(0x3A, <<0x2E>>)
      {:error, :name_taken} -> send_packet(0x3A, <<0x31>>)
      {:error, :name_missing} -> send_packet(0x3A, <<0x45>>)
      {:error, :name_too_short} -> send_packet(0x3A, <<0x46>>)
      {:error, :name_too_long} -> send_packet(0x3A, <<0x47>>)
      {:error, :name_invalid} -> send_packet(0x3A, <<0x48>>)
      {:error, _} -> send_packet(0x3A, <<0x51>>)
    end
    :ok
  end

  def handle_char_delete(payload, state) do
    << character_id :: little-size(64) >> = payload
    case Characters.delete_character(character_id, state.account.id) do
      :ok -> send_packet(0x3C, <<0x39>>)
      {:error, _} -> send_packet(0x3C, <<0x3A>>)
    end
  end

  def handle_player_login(<< character_id :: little-size(64) >>, state) do
    with %Character{} = c <- Characters.get_character(character_id, state.account.id)
    do
      set_character(c)
      payload = << c.map :: little-size(32), # map
                   c.position_x :: little-float-size(32), # x
                   c.position_y :: little-float-size(32), # y
                   c.position_z :: little-float-size(32), # z
                   c.orientation :: little-float-size(32) >>
      send_packet(0x236, payload) # SMSG_LOGIN_VERIFY_WORLD
      # send_packet(0x209, << 0 :: little-size(1024) >>) # SMSG_ACCOUNT_DATA_TIMES
      # send_packet(0x42, << 0 :: little-size(32), 0.01666667 :: float-size(32) >>) # SMSG_LOGIN_SETTIMESPEED 0.16667 is gamespeed
      send_packet(0xFD, << 0 :: little-size(256)>>) # SMSG_TUTORIAL_FLAGS
      send_packet(0xA9, spawn_packet(c))
    else
      nil -> exit(:normal)
    end
  end

  defp spawn_packet(%Character{} = character) do
    << # Update Object Packet Header
       1 :: little-size(32),
       0 :: little-size(8), # 32?
       3 :: little-size(8), # update type
       << 1 :: little-size(8), 4 :: little-size(8) >>, # PACKED GUID
       # character.id :: little-size(64),

       # Object Info
       4 :: little-size(8), # object type

      #  0x01 ||| 0x10 ||| 0x20 :: little-size(8),
      0x71 :: little-size(8),

       # Movement Info
       0 :: little-size(32), # Movement flags
       0 :: little-size(32), # time
       character.position_x :: little-float-size(32),
       character.position_y :: little-float-size(32),
       character.position_z :: little-float-size(32),
       character.orientation :: little-float-size(32),
       0 :: little-size(32), # Fall Time
       1.0 :: little-float-size(32), # walk speed
       7.0 :: little-float-size(32), # run speed
       4.5 :: little-float-size(32), # run backwards
       0.0 :: little-float-size(32), # swim speed
       0.0 :: little-float-size(32), # swim backwards


       #  219 :: little-size(8), 15 :: little-size(8), 73 :: little-size(8), 64 :: little-size(8), # turn rate 3.1415 (to match example packet)
       3.14159 :: little-float-size(32), # turn rate

       1 :: little-size(8), # is player

       1 :: little-size(8), # unk
       0 :: little-size(8), # unk
       0 :: little-size(8) # unk
       >> <> get_spawn_fields(character)

      #  41 :: little-size(8), # number of u32 mask blocks that will follow

      #  # Start of mask blocks
      #  23 :: little-size(8), 0 :: little-size(8), 64 :: little-size(8), 16 :: little-size(8),
      #  28 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  24 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),

      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),

      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),

      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),

      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),

      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),

      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),

      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),

      #  0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8), 0 :: little-size(8),
      #  # End of mask blocks

      #  4 :: little-size(64), # OBJECT_FIELD_GUID (4) (notice unpacked u64)
      #  25 :: little-size(32), # OBJECT_FIELD_TYPE (16 | 8 | 1) (TYPE_PLAYER | TYPE_UNIT | TYPE_OBJECT)
      #  1.0 :: little-float-size(32), # Scale
      #  100 :: little-size(32), # UNIT_FIELD_HEALTH
      #  100 :: little-size(32), # UNIT_FIELD_MAXHEALTH
      #  1 :: little-size(32), # UNIT_FIELD_LEVEL
      #  1 :: little-size(32), # UNIT_FIELD_FACTIONTEMPLATE

      #  character.race :: little-size(8),
      #  character.class :: little-size(8),
      #  character.gender :: little-size(8),
      #  1 :: little-size(8), # Power (Rage, figure this out based on class/transforms?)

      #  50 :: little-size(32), # UNIT_FIELD_DISPLAYID
      #  50 :: little-size(32) # UNIT_FIELD_NATIVEDISPLAYID
    #>>
  end

  defp get_spawn_fields(%Character{} = c) do
    {bit_mask, fields} = add_spawn_field(:id, 4)
    |> add_spawn_field(:type, 25)
    |> add_spawn_field(:scale_x, 1.0)
    |> add_spawn_field(:base_health, 20)
    |> add_spawn_field(:health, 100)
    |> add_spawn_field(:max_health, 100)
    |> add_spawn_field(:level, 1)
    |> add_spawn_field(:faction_template, 1)
    |> add_spawn_field(:unit_bytes_0, << c.race :: little-size(8), c.class :: little-size(8), c.gender :: little-size(8), 1 :: little-size(8) >>)
    |> add_spawn_field(:display_id, 50)
    |> add_spawn_field(:native_display_id, 50)


    bit_mask_binary = :binary.encode_unsigned(bit_mask, :little)

    adjusted_size = byte_size(bit_mask_binary) / 4
    |> ceil()
    |> (&(&1*4)).()

    bit_mask_binary = bit_mask_binary
    |> pad_trailing(adjusted_size)

    mask_blocks = adjusted_size / 4 |> trunc()

    Logger.debug "[serverd] Bitmask #{inspect(bit_mask_binary)}"
    Logger.debug "[serverd] Bitmask Length #{byte_size(bit_mask_binary)}"

    keys = Enum.sort(Map.keys(fields))
    fields_binary = Enum.reduce(keys, << >>, fn(key, acc) ->
      value = Map.get(fields, key)
      Logger.debug "[serverd] Field #{key}: #{inspect(value)}"
      acc <> value
    end)

    << mask_blocks :: little-size(8) >> <> bit_mask_binary <> fields_binary

  end

  defp add_spawn_field(field, value), do: add_spawn_field({0, %{}}, field, value)
  defp add_spawn_field({bit_mask, fields} = _, field, value) do
    Logger.debug "[serverd] Adding field #{field}: #{value}"
    {field_offset, field_type} = Fields.object_field(field)
    new_bit_mask = bit_mask ||| bit_mask(field_type) <<< field_offset
    {new_bit_mask, Map.put(fields, field_offset, serialize(field_type, value))}
  end

  defp bit_mask(:int32), do: 1
  defp bit_mask(:int64), do: 3
  defp bit_mask(:float), do: 1
  defp bit_mask(:four_bytes), do: 1
  defp bit_mask(:two_int16), do: 1

  defp serialize(:int32, value), do: << value :: little-size(32) >>
  defp serialize(:int64, value), do: << value :: little-size(64) >>
  defp serialize(:float, value), do: << value :: little-float-size(32) >>
  defp serialize(:four_bytes, value), do: value
  defp serialize(:two_int16, value), do: << value :: little-size(32) >>

  def handle_cmd({command, payload}, state) do
    Logger.debug "[serverd] Recv Command: #{inspect(command, base: :hex)}, Payload: #{inspect(payload, limit: :infinity)}"
    case command do
      0x1ED  -> handle_auth(payload, state) # CMSG_AUTH_SESSION
      0x1DC  -> handle_ping(payload, state) # CMSG_PING
      0x37 -> handle_char_enum(payload, state) # CMSG_CHAR_ENUM
      0x36 -> handle_char_create(payload, state) # CMSG_CHAR_CREATE
      0x38 -> handle_char_delete(payload, state) # CMSG_CHAR_DELETE
      0x3D -> handle_player_login(payload, state) # CMSG_PLAYER_LOGIN
      _   -> true # Logger.error "[serverd] Invalid command #{inspect(command, base: :hex)}, Header: #{inspect(header, limit: :infinity)}"
    end
  end

  def handle_packet(%{packet_stream: << header :: bytes-size(6), rest :: binary >> } = state) do
    {<<size :: big-size(16), command :: little-size(32) >>, crypt_state } = decrypt_header(header, state.crypt)

    payload_size = size - 4
    case rest do
      # If we have enough bytes to fill the message (as defined by the size), then handle it and process the rest as a separate message
      << payload :: bytes-size(payload_size), rest :: binary >> ->
        handle_cmd({command, payload}, state)
        handle_packet(Map.merge(state, %{crypt: crypt_state, packet_stream: rest}))

      # If we didn't match above, then we didn't have enough bytes and we're not handling a full message.
      # Keep the packet_stream the same and we'll append the next packet to it for further processing.
      _ -> state
    end
  end

  def handle_packet(%{packet_stream: <<>> } = state), do: Map.merge(state, %{packet_stream: <<>>})

  def handle_info({:tcp, _socket, packet}, state) do
    {:noreply, Map.merge(state, handle_packet(Map.merge(state, %{ packet_stream: state.packet_stream <> packet })))}
  end

  def handle_info({:tcp_closed, _socket}, _state) do
    Logger.debug "[serverd] Connection closed"
    exit(:normal)
  end

  def handle_cast({:init_encryption, key}, state) do
    crypt = %{ key: key,
               send_i: 0,
               send_j: 0,
               recv_i: 0,
               recv_j: 0,
               initialized: :true}
    {:noreply, Map.merge(state, %{crypt: crypt})}
  end

  def handle_cast({:set_account, account}, state) do
    {:noreply, Map.merge(state, %{account: account})}
  end

  def handle_cast({:set_character, character}, state) do
    {:noreply, Map.merge(state, %{character: character})}
  end

  def handle_cast({:set_latency, latency, last_ping}, state) do
    {:noreply, Map.merge(state, %{latency: latency, last_ping: last_ping})}
  end

  def handle_cast({:send_packet, {type, op_code, payload}}, state) do
    size = byte_size(payload) + 2
    header = << size :: big-size(16), op_code :: little-size(16) >>

    {encrypted_header, crypt_state} = encrypt_header(header, state.crypt)
    Logger.debug "[serverd] Send packet Command: #{inspect(op_code, base: :hex)}, Payload: #{inspect(payload, limit: :infinity)}"

    :gen_tcp.send(state.socket, encrypted_header <> payload)
    case type do
      :ok -> {:noreply, Map.merge(state, %{crypt: crypt_state})}
      :error -> exit(:normal)
    end
  end

  def handle_cast(:smsg_send_auth, state) do
    send_packet(0x1EC, state.seed)
    {:noreply, state}
  end
end
